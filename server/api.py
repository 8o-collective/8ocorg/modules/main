from . import app

from . import discord
from . import dna


@app.get('/api/invite')
def invite():
    """
    Returns an encrypted code and the key used to encrypt it.
    """
    key = dna.get_key()
    code = discord.get_code()
    encrypted = dna.encrypt(code, ''.join(key))

    # We should check if one IP is generating a lot of IDs and invite codes
    # client_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)

    return {
        "key": key,
        "encrypted": encrypted
    }
