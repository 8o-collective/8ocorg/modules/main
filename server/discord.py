import os
import urllib.request
import json

mode = os.environ.get('BRANCH', 'development')

appDirectory = os.path.dirname(os.path.realpath(__file__))

try:
    with open(os.path.join(appDirectory, "auth.json"), 'r', encoding='ascii') as f:
        auth = json.load(f)
except FileNotFoundError as e:
    raise PermissionError(
        """You do not have an authentication file.

		Please load auth.json from the AUTH project variable."""
    ) from e

with open(os.path.join(appDirectory, "wizx.config.json"), 'r', encoding='ascii') as f:
    config = json.load(f)

default_hdr = {
    'Authorization': f"Bot {auth['discord_token']}",
    'User-Agent': 'DiscordBot (8oc.org, v1)'
}


def get_code():
    """
    Returns a one-time use invite code to the discord server.
    """

    # https://discord.com/developers/docs/resources/channel#create-channel-invite
    data = json.dumps({
        'max_uses': 1,
        'unique': True,
        'max_age': 0 if mode == 'production' else 10  # for testing only
    }).encode('utf8')

    hdr = {
        **default_hdr,
        'Content-Type': 'application/json; charset=utf-8',
        'Content-Length': len(data)
    }

    url = f"https://discord.com/api/channels/{config['welcome_channel_id']}/invites"

    request = urllib.request.Request(
        url, headers=hdr, data=data, method='POST')
    with urllib.request.urlopen(request) as response:
        results = json.loads(response.read().decode('utf-8'))

    invite_code = results['code']
    # invite_link = 'discord.gg/' + invite_code

    return invite_code
