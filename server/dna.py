# pylint: disable=invalid-name
import ctypes
import itertools
import secrets
import math
import base36

KEY_LENGTH = 8          # 16
CYCLES = 2              # 32

DELTA = 0x9E3779B9      # ⌊(2**32)/ϕ⌋
SUM = DELTA * CYCLES


def get_key():
    """
    Generates a DNA cipher key array, consisting of 4 shards.

    :return:
            An array of length 4, containing strings of length 2.
    """
    def shard():
        return secrets.choice(['A', 'C', 'G', 'T']) + secrets.choice(['A', 'C', 'G', 'T'])

    return [shard(), shard(), shard(), shard()]


def encrypt(plaintext, key):
    """
    Encrypts a message using a key of length 4n.
    :param plaintext:
        The message to encode. *Must* be a utf8 string but can have any length.
    :param key:
        The encryption key used to encode the plaintext message.
        *Must* be a utf8 string and a multiple of 4.
    :return:
        A base36 utf8 string of the encrypted message.
    """
    if not plaintext:
        return ''

    v = _str2vec(plaintext.encode())
    k = _str2vec(key.encode()[:KEY_LENGTH], KEY_LENGTH // 4)

    b_array = b''.join(_vec2str(_encipher(chunk, k))
                       for chunk in _chunks(v, 2))

    return base36.dumps(int.from_bytes(b_array, 'big'))


def decrypt(ciphertext, key):
    """
    Decrypts a message using a key of length 4n.
    :param ciphertext:
        The encrypted message to decode as a base36 utf8 string.
    :param key:
        The encryption key used to encode the plaintext message.
        *Must* be a utf8 string and a multiple of 4.
    :return:
        A utf8 string of the decrypted message.
    """
    if not ciphertext:
        return ''

    k = _str2vec(key.encode()[:KEY_LENGTH], KEY_LENGTH // 4)
    v = _str2vec(base36.loads(int.from_bytes(ciphertext.encode(), 'big')))

    return b''.join(_vec2str(_decipher(chunk, k)) for chunk in _chunks(v, 2)).decode()


def _encipher(v, k):
    """
    TEA encipher algorithm.

    Encodes a length-2 vector using a length-4 vector as a length-2 vector.

    Compliment of _decipher.
    :param v:
        A vector representing the information to be enciphered.
        *Must* have a length of 2.
    :param k:
        A vector representing the encryption key.
        *Must* have a length of 4.
    :return:
        A length-2 vector representing the encrypted information v.
    """
    y, z = [ctypes.c_uint32(sb) for sb in v]
    c_sum = ctypes.c_uint32(0)

    for _ in range(CYCLES):
        c_sum.value += DELTA
        y.value += (z.value << 4) + k[0] ^ z.value + \
            c_sum.value ^ (z.value >> 5) + k[1]
        z.value += (y.value << 4) + k[2] ^ y.value + \
            c_sum.value ^ (y.value >> 5) + k[3]

    return [y.value, z.value]


def _decipher(v, k):
    """
    TEA decipher algorithm. Decodes a length-2 vector using a length-4 vector as a length-2 vector.

    Compliment of _encipher.
    :param v:
        A vector representing the information to be deciphered.  *Must* have a length of 2.
    :param k:
        A vector representing the encryption key.  *Must* have a length of 4.
    :return:
        The original message.
    """
    y, z = [ctypes.c_uint32(sb) for sb in v]
    c_sum = ctypes.c_uint32(SUM)

    for _ in range(CYCLES):
        z.value -= (y.value << 4) + k[2] ^ y.value + \
            c_sum.value ^ (y.value >> 5) + k[3]
        y.value -= (z.value << 4) + k[0] ^ z.value + \
            c_sum.value ^ (z.value >> 5) + k[1]
        c_sum.value -= DELTA

    return [y.value, z.value]


def _chunks(iterable, n):
    """
    Iterates through an iterable in chunks of size n.
    :param iterable:
        Any iterable.
        Must have a length which is a multiple of n, or the last element will
        not contain n elements.
    :param n:
        The size of the chunks.
    :return:
        A generator that yields elements in chunks of size n.
    """
    it = iter(iterable)
    while True:
        chunk = tuple(itertools.islice(it, n))
        if not chunk:
            return
        yield chunk


def _str2vec(value, l=4):
    """
    Encodes a binary string as a vector.

    The string is split into chunks of length l and each chunk is encoded as
    2 elements in the return value.

    Compliment of _vec2str.

    :param value:
        A binary string to encode.
    :param l:
        An optional length value of chunks.
    :return:
        A vector containing ceil(n / l) elements where n is the length of the value parameter.
    """
    n = len(value)

    # Split the string into chunks
    num_chunks = math.ceil(n / l)
    chunks = [value[l * i:l * (i + 1)] for i in range(num_chunks)]

    return [sum((character << 8 * j for j, character in enumerate(chunk))) for chunk in chunks]


def _vec2str(vector, l=4):
    """
    Decodes a vector to a binary string.
    The string is composed by chunks of size l for every two elements in the vector.

    Compliment of _str2vec.

    :param vector:
        An even-length vector.
    :param l:
        The length of the chunks to compose the returned string.
        This should match the value for l used by _str2vec.
        If the value used is smaller, then characters will be lost.
    :return:
    """
    return bytes(
        (element >> 8 * i) & 0xff for element in vector for i in range(l)
    ).replace(b'\x00', b'')
