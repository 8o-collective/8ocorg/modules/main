import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter as Router } from "react-router-dom";

import { routes } from "routes.js";
import { PuzzleProvider } from "contexts/Puzzle/provider.js";

// render the main component
ReactDOM.createRoot(document.getElementById("root")).render(
  <PuzzleProvider>
    <Router>{routes}</Router>
  </PuzzleProvider>
);
