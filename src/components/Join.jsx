import React, { useState, useEffect, useRef, useContext } from "react";

import {
  JoinContainer,
  JoinCanvas,
  JoinButtonContainer,
  JoinOutline,
  JoinOutlineButton,
} from "assets/styles/Join.styles.jsx";

import { PuzzleContext } from "contexts/Puzzle/provider.js";

import frameRenderer from "actions/join.renderer.js";

import constants from "assets/constants.js";

const START_TIME =
  (constants.mission.time + constants.mission.fadeout) *
  constants.mission.fakes;
const UNLOCK_OUTLINE =
  Math.floor(Math.random() * constants.join.button.outlines) + 1;
const PORTRAIT_DISPLAY = window.innerHeight > window.innerWidth;
const SPEED_MULTIPLIER = PORTRAIT_DISPLAY ? 0.6 : 1;

const size = {
  // width: (constants.join.width * window.innerWidth) / 100,
  width: PORTRAIT_DISPLAY ? window.innerWidth : constants.join.width,
  height: PORTRAIT_DISPLAY ? constants.join.height : window.innerHeight,
};

const calculateDimensions = () => {
  const getInsets = (boxDimensions) => ({
    top: boxDimensions.center.y - Math.floor(boxDimensions.height / 2),
    bottom: boxDimensions.center.y + Math.floor(boxDimensions.height / 2),
    left: boxDimensions.center.x - Math.floor(boxDimensions.width / 2),
    right: boxDimensions.center.x + Math.floor(boxDimensions.width / 2),
  });

  if (PORTRAIT_DISPLAY) {
    const dimensions = {
      outerBox: {
        // defines the structure drawing, which meets it and then draws around it
        height: Math.floor(size.height / 3),
        width: Math.floor(size.width / 2),
        center: {
          x: (size.width * 4) / 11,
          y: size.height / 2,
        },
      },
      innerBox: {
        // defines the box in which the outlined join button sits
        height: Math.floor(size.height / 2),
        width: Math.floor(size.width / 3),
        center: {
          x: (size.width * 2) / 5,
          y: size.height / 2,
        },
      },
    };

    const insets = {
      outerBox: getInsets(dimensions.outerBox),
      innerBox: getInsets(dimensions.innerBox),
    };

    // starting coordinates of the lines
    const coords = {
      leftmost: {
        x: 0,
        y: (7 * size.height) / 10,
      },
      rightmost: {
        x: size.width,
        y: (1 * size.height) / 10,
      },
    };

    const ends = {
      leftmost: {
        x: insets.innerBox.left,
        y: insets.outerBox.top,
      },
      rightmost: {
        x: insets.innerBox.right,
        y: insets.outerBox.bottom,
      },
    };

    // anything ending in a + is a destination
    const distances = {
      structure: {
        leftmost:
          insets.outerBox.left - // leg 1
          coords.leftmost.x +
          coords.leftmost.y - // leg 2
          insets.outerBox.top +
          insets.innerBox.left - // leg 3
          insets.outerBox.left,
        rightmost:
          coords.rightmost.x - // leg 1
          insets.outerBox.right +
          insets.outerBox.bottom - // leg 2
          coords.rightmost.y +
          insets.outerBox.right - // leg 3
          insets.innerBox.right,
      },
      box: {
        leftmost: {
          right:
            insets.outerBox.top - // leg 1
            insets.innerBox.top +
            dimensions.innerBox.width, // leg 2
          down: insets.outerBox.bottom - insets.innerBox.top,
        },
        rightmost: {
          left:
            insets.innerBox.bottom - // leg 1
            insets.outerBox.bottom +
            dimensions.innerBox.width, // leg 2
          up: insets.innerBox.bottom - insets.outerBox.top,
        },
      },
    };

    const structure = {
      draw: true,
      leftmost: {
        coords: structuredClone(coords.leftmost),
        last: structuredClone(coords.leftmost),
        speed:
          Math.max(
            distances.structure.leftmost / distances.structure.rightmost,
            1
          ) * SPEED_MULTIPLIER,
      },
      rightmost: {
        coords: structuredClone(coords.rightmost),
        last: structuredClone(coords.rightmost),
        speed:
          Math.max(
            distances.structure.rightmost / distances.structure.leftmost,
            1
          ) * SPEED_MULTIPLIER,
      },
    };

    const box = {
      draw: false,
      leftmost: {
        right: {
          // up and right, 2 legs
          coords: structuredClone(ends.leftmost),
          last: structuredClone(ends.leftmost),
          speed:
            Math.max(
              distances.box.leftmost.right / distances.box.leftmost.down,
              1
            ) * SPEED_MULTIPLIER,
        },
        down: {
          // just down, one leg
          coords: structuredClone(ends.leftmost),
          last: structuredClone(ends.leftmost),
          speed:
            Math.max(
              distances.box.leftmost.down / distances.box.leftmost.right,
              1
            ) * SPEED_MULTIPLIER,
        },
      },
      rightmost: {
        left: {
          // down and left, 2 legs
          coords: structuredClone(ends.rightmost),
          last: structuredClone(ends.rightmost),
          speed:
            Math.max(
              distances.box.rightmost.left / distances.box.rightmost.up,
              1
            ) * SPEED_MULTIPLIER,
        },
        up: {
          // just up, one leg
          coords: structuredClone(ends.rightmost),
          last: structuredClone(ends.rightmost),
          speed:
            Math.max(
              distances.box.rightmost.up / distances.box.rightmost.left,
              1
            ) * SPEED_MULTIPLIER,
        },
      },
    };

    // join is joinRef.current
    const computeJoin = (join) => {
      if (join.structure.draw) {
        join.structure.leftmost.last = structuredClone(
          join.structure.leftmost.coords
        );
        join.structure.rightmost.last = structuredClone(
          join.structure.rightmost.coords
        );

        if (join.structure.leftmost.coords.x < insets.outerBox.left) {
          join.structure.leftmost.coords.x += join.structure.leftmost.speed; // leg 1
        } else if (join.structure.leftmost.coords.y > insets.outerBox.top) {
          join.structure.leftmost.coords.y -= join.structure.leftmost.speed; // leg 2
        } else if (join.structure.leftmost.coords.x < insets.innerBox.left) {
          join.structure.leftmost.coords.x += join.structure.leftmost.speed; // leg 3
        }

        if (join.structure.rightmost.coords.x > insets.outerBox.right) {
          join.structure.rightmost.coords.x -= join.structure.rightmost.speed; // leg 1
        } else if (join.structure.rightmost.coords.y < insets.outerBox.bottom) {
          join.structure.rightmost.coords.y += join.structure.rightmost.speed; // leg 2
        } else if (join.structure.rightmost.coords.x > insets.innerBox.right) {
          join.structure.rightmost.coords.x -= join.structure.rightmost.speed; // leg 3
        } else {
          join.structure.draw = false;
          join.box.draw = true;
        }
      } else if (join.box.draw) {
        join.box.leftmost.right.last = structuredClone(
          join.box.leftmost.right.coords
        );
        join.box.leftmost.down.last = structuredClone(
          join.box.leftmost.down.coords
        );
        join.box.rightmost.left.last = structuredClone(
          join.box.rightmost.left.coords
        );
        join.box.rightmost.up.last = structuredClone(
          join.box.rightmost.up.coords
        );

        if (join.box.leftmost.down.coords.y < insets.innerBox.bottom) {
          join.box.leftmost.down.coords.y += join.box.leftmost.down.speed;
        }

        if (join.box.rightmost.up.coords.y > insets.innerBox.top) {
          join.box.rightmost.up.coords.y -= join.box.rightmost.up.speed;
        }

        if (join.box.leftmost.right.coords.y > insets.innerBox.top) {
          join.box.leftmost.right.coords.y -= join.box.leftmost.right.speed; // leg 1
        } else if (join.box.leftmost.right.coords.x < insets.innerBox.right) {
          join.box.leftmost.right.coords.x += join.box.leftmost.right.speed; // leg 2
        }

        if (join.box.rightmost.left.coords.y < insets.innerBox.bottom) {
          join.box.rightmost.left.coords.y += join.box.rightmost.left.speed; // leg 1
        } else if (join.box.rightmost.left.coords.x > insets.innerBox.left) {
          join.box.rightmost.left.coords.x -= join.box.rightmost.left.speed; // leg 2
        } else {
          join.box.draw = false;
          join.done = true;
        }
      }
    };

    return {
      dimensions,
      structure,
      box,
      computeJoin,
    };
  } else {
    const dimensions = {
      outerBox: {
        // defines the structure drawing, which meets it and then draws around it
        height: Math.floor(size.height / 3),
        width: Math.floor(size.width / 3),
        center: {
          x: size.width / 2,
          y: (size.height * 2) / 3,
        },
      },
      innerBox: {
        // defines the box in which the outlined join button sits
        height: Math.floor(size.height / 6),
        width: Math.floor(size.width / 2),
        center: {
          x: size.width / 2,
          y: (size.height * 3) / 5,
        },
      },
    };

    const insets = {
      outerBox: getInsets(dimensions.outerBox),
      innerBox: getInsets(dimensions.innerBox),
    };

    const coords = {
      upper: {
        x: (9 * size.width) / 10,
        y: 0,
      },
      lower: {
        x: (1 * size.width) / 10,
        y: size.height,
      },
    };

    const ends = {
      upper: {
        x: insets.outerBox.left,
        y: insets.innerBox.top,
      },
      lower: {
        x: insets.outerBox.right,
        y: insets.innerBox.bottom,
      },
    };

    const distances = {
      structure: {
        upper:
          insets.outerBox.top - // leg 1
          coords.upper.y +
          coords.upper.x - // leg 2
          insets.outerBox.left +
          insets.innerBox.top - // leg 3
          insets.outerBox.top,
        lower:
          coords.lower.y - // leg 1
          insets.outerBox.bottom +
          insets.outerBox.right - // leg 2
          coords.lower.x +
          insets.outerBox.bottom - // leg 3
          insets.innerBox.bottom,
      },
      box: {
        upper: {
          down:
            insets.outerBox.left - // leg 1
            insets.innerBox.left +
            dimensions.innerBox.height, // leg 2
          right: insets.innerBox.right - insets.outerBox.left,
        },
        lower: {
          up:
            insets.innerBox.right - // leg 1
            insets.outerBox.right +
            dimensions.innerBox.height, // leg 2
          left: insets.outerBox.right - insets.innerBox.left,
        },
      },
    };

    const structure = {
      draw: true,
      upper: {
        coords: structuredClone(coords.upper),
        last: structuredClone(coords.upper),
        speed:
          Math.max(distances.structure.upper / distances.structure.lower, 1) *
          SPEED_MULTIPLIER,
      },
      lower: {
        coords: structuredClone(coords.lower),
        last: structuredClone(coords.lower),
        speed:
          Math.max(distances.structure.lower / distances.structure.upper, 1) *
          SPEED_MULTIPLIER,
      },
    };

    const box = {
      draw: false,
      upper: {
        down: {
          // left and down, 2 legs
          coords: structuredClone(ends.upper),
          last: structuredClone(ends.upper),
          speed:
            Math.max(distances.box.upper.down / distances.box.upper.right, 1) *
            SPEED_MULTIPLIER,
        },
        right: {
          // just right, one leg
          coords: structuredClone(ends.upper),
          last: structuredClone(ends.upper),
          speed:
            Math.max(distances.box.upper.right / distances.box.upper.down, 1) *
            SPEED_MULTIPLIER,
        },
      },
      lower: {
        up: {
          // right and up, 2 legs
          coords: structuredClone(ends.lower),
          last: structuredClone(ends.lower),
          speed:
            Math.max(distances.box.lower.up / distances.box.lower.left, 1) *
            SPEED_MULTIPLIER,
        },
        left: {
          // just left, one leg
          coords: structuredClone(ends.lower),
          last: structuredClone(ends.lower),
          speed:
            Math.max(distances.box.lower.left / distances.box.lower.up, 1) *
            SPEED_MULTIPLIER,
        },
      },
    };

    // join is joinRef.current
    const computeJoin = (join) => {
      if (join.structure.draw) {
        join.structure.upper.last = structuredClone(
          join.structure.upper.coords
        );
        join.structure.lower.last = structuredClone(
          join.structure.lower.coords
        );

        if (join.structure.upper.coords.y < insets.outerBox.top) {
          join.structure.upper.coords.y += join.structure.upper.speed; // leg 1
        } else if (join.structure.upper.coords.x > insets.outerBox.left) {
          join.structure.upper.coords.x -= join.structure.upper.speed; // leg 2
        } else if (join.structure.upper.coords.y < insets.innerBox.top) {
          join.structure.upper.coords.y += join.structure.upper.speed; // leg 3
        }

        if (join.structure.lower.coords.y > insets.outerBox.bottom) {
          join.structure.lower.coords.y -= join.structure.lower.speed; // leg 1
        } else if (join.structure.lower.coords.x < insets.outerBox.right) {
          join.structure.lower.coords.x += join.structure.lower.speed; // leg 2
        } else if (join.structure.lower.coords.y > insets.innerBox.bottom) {
          join.structure.lower.coords.y -= join.structure.lower.speed; // leg 3
        } else {
          join.structure.draw = false;
          join.box.draw = true;
        }
      } else if (join.box.draw) {
        join.box.upper.down.last = structuredClone(join.box.upper.down.coords);
        join.box.upper.right.last = structuredClone(
          join.box.upper.right.coords
        );
        join.box.lower.up.last = structuredClone(join.box.lower.up.coords);
        join.box.lower.left.last = structuredClone(join.box.lower.left.coords);

        if (join.box.upper.right.coords.x < insets.innerBox.right) {
          join.box.upper.right.coords.x += join.box.upper.right.speed;
        }

        if (join.box.lower.left.coords.x > insets.innerBox.left) {
          join.box.lower.left.coords.x -= join.box.lower.left.speed;
        }

        if (join.box.upper.down.coords.x > insets.innerBox.left) {
          join.box.upper.down.coords.x -= join.box.upper.down.speed; // leg 1
        } else if (join.box.upper.down.coords.y < insets.innerBox.bottom) {
          join.box.upper.down.coords.y += join.box.upper.down.speed; // leg 2
        }

        if (join.box.lower.up.coords.x < insets.innerBox.right) {
          join.box.lower.up.coords.x += join.box.lower.up.speed; // leg 1
        } else if (join.box.lower.up.coords.y > insets.innerBox.top) {
          join.box.lower.up.coords.y -= join.box.lower.up.speed; // leg 2
        } else {
          join.box.draw = false;
          join.done = true;
        }
      }
    };

    return {
      dimensions,
      structure,
      box,
      computeJoin,
    };
  }
};

const JoinButton = ({ height, width, center }) => {
  const [state, dispatch] = useContext(PuzzleContext);
  const [flashArray, setFlashArray] = useState(
    Array(constants.join.button.outlines + 1).fill(false)
  );
  const paused = useRef(false);
  const unlockTimer = useRef(false);

  const setJoinMission = (mission) =>
    dispatch({
      join: {
        mission,
      },
    });

  const incrementPuzzleTo = (i) =>
    dispatch({
      stage: state.stage <= i ? i : state.stage,
    });

  const setFlashArrayValue = (i, v) => {
    setFlashArray((prevFlashArray) => [
      ...prevFlashArray.slice(0, i),
      v,
      ...prevFlashArray.slice(i + 1),
    ]);
  };

  useEffect(() => {
    setFlashArray((prevFlashArray) => prevFlashArray.fill(false));

    const intervals = flashArray.map((e, i) =>
      setTimeout(() => {
        setInterval(() => {
          !paused.current && setFlashArrayValue(i, true);
          setTimeout(() => {
            !paused.current && setFlashArrayValue(i, false);
          }, constants.join.button.flash);
        }, constants.join.button.loop);
      }, (i * constants.join.button.loop) / constants.join.button.outlines)
    );

    return () => intervals.forEach((interval) => clearInterval(interval));
  }, [paused]);

  // useEffect(() => {
  //   setFlashArray(prevFlashArray => prevFlashArray.fill(false))
  // }, [paused])

  const outlinedButtonDimensions = {
    height: height / 4,
    width: width / 2,
  };

  const handleOutlineMouseOver = (e, i) => {
    e.stopPropagation(); // prevent propagation to parent outline
    if (i === UNLOCK_OUTLINE) {
      setJoinMission(null);
      unlockTimer.current = setTimeout(
        () => incrementPuzzleTo(1),
        constants.join.unlock
      );
    } else {
      setJoinMission(i + 3); // skip 3 missions
    }
    paused.current = true;
    setFlashArrayValue(i, null);
  };

  const handleOutlineMouseOut = (e, i) => {
    clearTimeout(unlockTimer.current);
    setFlashArrayValue(i, false);
    setJoinMission(true);
    paused.current = false;
  };

  const handleButtonMouseOver = (e) => {
    e.stopPropagation();
    setJoinMission(true);
  };

  const handleButtonMouseOut = (e) => {
    handleOutlineMouseOut(e, 1);
  };

  return (
    <JoinButtonContainer
      style={{
        top: `${center.y}px`,
        left: `${center.x}px`,
        height: `${height}px`,
        width: `${width}px`,
      }}
      flash={flashArray[0]}
    >
      {Array.from(
        { length: constants.join.button.outlines },
        (v, i) => i + 1
      ).reduce(
        (acc, curr) => (
          <JoinOutline
            flash={flashArray[curr]}
            style={{
              // calculate padding by subtracting button dimensions and borders of each outline
              // form total dimenions. Then divide by 2 to get padding for each side and divide by the
              // number of outlines to get the padding for each outline
              padding: `${
                (height -
                  outlinedButtonDimensions.height -
                  2 * constants.join.button.outlines) / // factor in the border
                (2 * constants.join.button.outlines)
              }px ${
                (width -
                  outlinedButtonDimensions.width -
                  2 * constants.join.button.outlines) / // factor in the border
                (2 * constants.join.button.outlines)
              }px`,
            }}
            onMouseOver={(e) => handleOutlineMouseOver(e, curr)}
            onMouseOut={(e) => handleOutlineMouseOut(e, curr)}
          >
            {acc}
          </JoinOutline>
        ),
        <JoinOutlineButton
          style={{
            height: `${outlinedButtonDimensions.height}px`,
            lineHeight: `${outlinedButtonDimensions.height}px`,
            fontSize: `${outlinedButtonDimensions.height}px`,
          }}
          onMouseOver={(e) => handleButtonMouseOver(e)}
          onMouseOut={(e) => handleButtonMouseOut(e)}
        >
          Join
        </JoinOutlineButton>
      )}
    </JoinButtonContainer>
  );
};

const Join = (props) => {
  const [start, setStart] = useState(true);
  const [button, setButton] = useState(false);

  const canvasRef = useRef(null);
  const requestIdRef = useRef(null);

  const { dimensions, structure, box, computeJoin } = calculateDimensions();

  const joinRef = useRef({
    done: false,
    structure,
    box,
  });

  useEffect(() => {
    start && beginAnimation();
  }, [start]);

  const beginAnimation = () => {
    joinRef.current.done = false;

    requestIdRef.current = requestAnimationFrame(tick);
  };

  const renderFrame = () => {
    computeJoin(joinRef.current);

    if (joinRef.current.done) {
      setButton(true);
    }

    const ctx = canvasRef.current.getContext("2d");
    frameRenderer.call(ctx, joinRef.current, PORTRAIT_DISPLAY);
  };

  const tick = () => {
    if (!canvasRef.current) return;

    if (!joinRef.current.done) {
      renderFrame();
      requestIdRef.current = requestAnimationFrame(tick);
    } else {
      cancelAnimationFrame(requestIdRef.current);
    }
  };

  useEffect(() => {
    const startTimer = setTimeout(() => setStart(true), START_TIME);

    return () => clearTimeout(startTimer);
  }, []);

  return (
    <JoinContainer>
      {button && <JoinButton {...dimensions.innerBox} {...props} />}
      <JoinCanvas {...size} ref={canvasRef} start={start ? 1 : 0} />
    </JoinContainer>
  );
};

export default Join;
