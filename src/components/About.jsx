import React from "react";

import {
  AboutContainer,
  AboutHeader,
  AboutQuestionsContainer,
  AboutQuestion,
  AboutAnswerDialog,
} from "assets/styles/About.styles.jsx";

import questions from "assets/questions.json";

const About = () => {
  return (
    <AboutContainer>
      <AboutHeader>About</AboutHeader>
      <AboutQuestionsContainer>
        {questions.map((entry, i) => (
          <AboutQuestion key={i}>
            {entry.question}
            <AboutAnswerDialog>{entry.answer}</AboutAnswerDialog>
          </AboutQuestion>
        ))}
      </AboutQuestionsContainer>
    </AboutContainer>
  );
};

export default About;
