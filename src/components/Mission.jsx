import React, { useState, useEffect, useContext } from "react";

import {
  MissionContainer,
  MissionSupportText,
  MissionText,
} from "assets/styles/Mission.styles.jsx";

import { PuzzleContext } from "contexts/Puzzle/provider.js";

import { GlitchText } from "assets/styles/Glitch.animations.jsx";

import missions from "assets/missions.json";
import constants from "assets/constants.js";

const shuffledFakes = missions.fakes
  .sort(() => 0.5 - Math.random())
  .filter(
    (fake) => window.innerHeight < window.innerWidth || fake.group.length < 20
  );

const Mission = () => {
  const [state] = useContext(PuzzleContext);

  const [done, setDone] = useState(false);
  const [enter, setEnter] = useState(false);
  const [glow, setGlow] = useState(false);
  const [mission, setMission] = useState({ group: null, aim: null });

  useEffect(() => {
    let missionCount = 0;

    const chooseMission = () => {
      setMission(shuffledFakes[missionCount]);
      missionCount++;
    };

    const interval = setInterval(() => {
      setTimeout(() => {
        if (missionCount == constants.mission.fakes) {
          setMission(missions.truth);
          setDone(true);
          setGlow(true);

          clearInterval(interval);
        } else {
          chooseMission();
        }

        setTimeout(() => setEnter(false), constants.mission.fadeout / 2);
      }, constants.mission.fadeout / 2);

      setEnter(true);
    }, constants.mission.time);

    chooseMission();

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (done) {
      setGlow(false);
      if (state.join.mission === null) {
        setMission(missions.unlock);
      } else {
        if (state.join.mission === true) {
          setGlow(true);
          setMission(missions.truth);
        } else {
          setMission(shuffledFakes[state.join.mission]);
        }
      }

      setEnter(true);
      setTimeout(() => setEnter(false), constants.mission.fadeout / 2);
    }
  }, [state.join.mission]);

  return (
    <MissionContainer>
      <MissionSupportText>8o-COLLECTIVE is</MissionSupportText>
      <MissionText glow={glow}>
        {state.join.mission !== null ? (
          <GlitchText num={10} enter={enter}>
            {mission.group}
          </GlitchText>
        ) : (
          <MissionSupportText>{mission.group}</MissionSupportText>
        )}
      </MissionText>
      <MissionSupportText>
        that supports and nurtures the development of
      </MissionSupportText>
      <MissionText glow={glow}>
        {state.join.mission !== null ? (
          <GlitchText num={10} enter={enter}>
            {mission.aim}.
          </GlitchText>
        ) : (
          <MissionSupportText>{mission.aim}.</MissionSupportText>
        )}
      </MissionText>
    </MissionContainer>
  );
};

export default Mission;
