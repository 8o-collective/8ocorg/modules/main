import React, { useContext } from "react";

import {
  ProjectsContainer,
  ProjectsHeader,
  ProjectsListContainer,
  ProjectList,
  ProjectDialog,
  ProjectLink,
} from "assets/styles/Projects.styles.jsx";

import { PuzzleContext } from "contexts/Puzzle/provider.js";

import puzzle from "assets/puzzle.json";

const Projects = () => {
  const [state, dispatch] = useContext(PuzzleContext);

  return (
    <ProjectsContainer>
      <ProjectsHeader>Projects</ProjectsHeader>
      <ProjectsListContainer>
        <ProjectList>
          <li>
            <ProjectLink>opus8</ProjectLink>
          </li>
          <li>
            <ProjectLink>titro</ProjectLink>
          </li>
          {state.stage >= 1 && (
            <li>
              <ProjectLink
                style={{ color: "black" }}
                onMouseEnter={() =>
                  dispatch({ stage: state.stage <= 2 ? 2 : state.stage })
                }
              >
                88888888
                <ProjectDialog>{puzzle.stage[1].dialog}</ProjectDialog>
              </ProjectLink>
            </li>
          )}
          <li>
            <ProjectLink>parable</ProjectLink>
          </li>
        </ProjectList>
      </ProjectsListContainer>
    </ProjectsContainer>
  );
};

export default Projects;
