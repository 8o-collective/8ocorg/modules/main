import React, { useEffect } from "react";
import generateEntry from "actions/generateEntry.js";

import { AppContainer } from "assets/styles/App.styles.jsx";

import About from "components/About.jsx";
import Join from "components/Join.jsx";
import Mission from "components/Mission.jsx";
import Projects from "components/Projects.jsx";

import Tear from "components/Puzzle/Tear.jsx";

const App = () => {
  useEffect(() => {
    if (localStorage.getItem("id")) return;

    generateEntry();
  }, []);

  return (
    <AppContainer>
      <About />
      <Join />
      <Mission />
      <Projects />

      <Tear />
    </AppContainer>
  );
};

export default App;
