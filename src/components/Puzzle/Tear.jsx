import React, { useContext } from "react";

import { TearContainer, TearImage } from "assets/styles/Puzzle/Tear.styles.jsx";

import { PuzzleContext } from "contexts/Puzzle/provider.js";

import tear from "assets/images/tear.png";

const Tear = () => {
  const [state] = useContext(PuzzleContext);

  return (
    state.stage >= 2 && (
      <TearContainer>
        <TearImage
          src={tear}
          onClick={() =>
            (window.location.href = `http://y8k.${window.location.host}`)
          }
        />
      </TearContainer>
    )
  );
};

export default Tear;
