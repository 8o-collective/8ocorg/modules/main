import React from "react";
import { Routes, Route } from "react-router-dom";

import App from "components/App.jsx";

const routes = (
  <Routes>
    <Route path="*" element={<App />} />
  </Routes>
);

export { routes };
