import styled from "styled-components";

const ProjectsContainer = styled.div`
  margin: 0px;
  position: absolute;
  height: 20vh;
  bottom: 15vh;
  right: 0px;
  border: 2px solid red;
  border-right: none;

  display: flex;
  flex-direction: column;

  writing-mode: vertical-lr;

  @media (max-width: 768px) {
    top: 55vh;
  }
`;

const ProjectsHeader = styled.div`
  border-left: 2px solid red;
  transform: rotate(180deg);

  font-size: max(1.5vw, 15px);
  text-align: center;
`;

const ProjectsListContainer = styled.div`
  writing-mode: horizontal-tb;
  height: 100%;
  padding-right: 2vw;
`;

const ProjectList = styled.ul`
  height: 100%;
`;

const ProjectDialog = styled.div`
  position: absolute;
  top: 100%;
  border: 1px solid red;
  transform: translate(-15vw, -60%);
  background-color: black;
  z-index: 2;

  padding: 1vw;

  display: none;
  color: white;

  &:after,
  &:before {
    left: 100%;
    top: 50%;
    border: solid transparent;
    content: "";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  &:after {
    border-color: rgba(0, 0, 0, 0);
    border-left-color: #000000;
    border-width: 15px;
    margin-top: -15px;
  }

  &:before {
    border-left-color: red;
    border-width: 16px;
    margin-top: -16px;
  }
`;

const ProjectLink = styled.a`
  color: white;
  cursor: pointer;
  font-size: max(1vw, 13px);

  &:hover {
    text-decoration: underline;
    color: red;
  }

  &:hover ${ProjectDialog} {
    display: block;
  }
`;

export {
  ProjectsContainer,
  ProjectsHeader,
  ProjectsListContainer,
  ProjectList,
  ProjectDialog,
  ProjectLink,
};
