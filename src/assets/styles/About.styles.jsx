import React from "react";
import styled from "styled-components";

const AboutContainer = styled.div`
  margin: 0px;
  position: absolute;
  width: max(30vw, 300px);
  bottom: 0px;
  left: 0px;

  display: flex;
  flex-direction: column;

  border: 2px solid red;
  border-left: 0px;
  border-bottom: 0px;

  @media (max-width: 768px) {
    bottom: -125px;
    width: 100%;
  }
`;

const AboutHeader = styled.div`
  width: 100%;
  border-bottom: 2px solid red;

  font-size: max(1.5vw, 17px);
  text-align: center;
`;

const AboutQuestionsContainer = styled.div`
  width: 100%;

  display: flex;
  flex-direction: column;
`;

const AboutAnswerDialog = styled.div`
  position: absolute;
  border: 1px solid red;
  transform: translate(max(31vw, 320px), -80%);
  background-color: black;
  z-index: 2;

  padding: max(1vw, 10px);

  display: none;
  color: white;

  &:after,
  &:before {
    right: 100%;
    top: 80%;
    border: solid transparent;
    content: "";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  &:after {
    border-color: rgba(0, 0, 0, 0);
    border-right-color: #000000;
    border-width: 15px;
    margin-top: -15px;
  }

  &:before {
    border-right-color: red;
    border-width: 16px;
    margin-top: -16px;
  }

  @media (max-width: 768px) {
    transform: translateY(-150%);

    &:after,
    &:before {
      top: 100%;
      left: 50%;
      border: solid transparent;
      content: "";
      height: 0;
      width: 0;
      position: absolute;
      pointer-events: none;

      // Reset styles
      right: initial;
      margin-top: initial;
    }

    &:after {
      border-color: rgba(0, 0, 0, 0);
      border-top-color: #000000;
      border-width: 15px;
      margin-left: -15px;
    }

    &:before {
      // border-color: red;
      border-top-color: red;
      border-width: 16px;
      margin-left: -16px;
    }
  }
`;

const AboutQuestionContainer = styled.div`
  padding: max(1vw, 10px);
  border-bottom: 1px solid red;

  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;

  font-size: max(1vw, 13px);

  cursor: pointer;

  &:hover,
  &:active {
    color: red;
  }

  &:hover ${AboutAnswerDialog}, &:active ${AboutAnswerDialog} {
    display: block;
  }

  @media (max-width: 768px) {
    justify-content: center;
  }
`;

const AboutQuestionChevron = styled.div`
  text-align: left;

  @media (max-width: 768px) {
    display: none;
  }
`;

const AboutQuestion = ({ children }) => (
  <AboutQuestionContainer ontouchstart="">
    {children}
    <AboutQuestionChevron>{">"}</AboutQuestionChevron>
  </AboutQuestionContainer>
);

export {
  AboutContainer,
  AboutHeader,
  AboutQuestionsContainer,
  AboutQuestion,
  AboutAnswerDialog,
};
