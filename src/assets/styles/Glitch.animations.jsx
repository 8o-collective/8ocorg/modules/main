import React from "react";
import styled, { css, keyframes } from "styled-components";

const GlitchClipAnimation = keyframes`
  0% {
    clip-path: polygon(
      0 100%,
      100% 100%,
      100% 120%,
      0 120%
    );
  }
  
  100% {
    clip-path: polygon(
      0 -20%,
      100% -20%,
      100% 0%,
      0 0
    );
  }
`;

let GlitchChildAnimation = keyframes`
    0% {
        transform: translateX(0);
    }
    80% {
        transform: translateX(0);
        color: #fff;
    }
    85% {
        transform: translateX(${Math.floor(Math.random() * 10) - 5}px);
        color: #4E9A26;
    }
    90% {
        transform: translateX(${Math.floor(Math.random() * 10) - 5}px);
        color: #AC1212;
    }
    95% {
        transform: translateX(${Math.floor(Math.random() * 10) - 5}px);
        color: #fff;
    }
    100% {
        transform: translateX(0);
    }
`;

let GlitchChildEnterAnimation = keyframes`
    0% {
        transform: translateX(0px);
    }
    50% {
        transform: translateX(0px);
        color: #fff;
    }
    ${[...Array(10).keys()].map(
      (i) =>
        `
            ${i + 60}% {
                transform: translateX(${
                  Math.floor(Math.random() * 250) - 125
                }px);
                color: #4E9A26;
            }
        `
    )}
    ${[...Array(10).keys()].map(
      (i) =>
        `
            ${i + 70}% {
                transform: translateX(${
                  Math.floor(Math.random() * 500) - 250
                }px);
                color: #fff;
            }
        `
    )}
    ${[...Array(10).keys()].map(
      (i) =>
        `
            ${i + 80}% {
                transform: translateX(${
                  Math.floor(Math.random() * 1000) - 500
                }px);
                color: #AC1212;
            }
        `
    )}
    ${[...Array(10).keys()].map(
      (i) =>
        `
            ${i + 90}% {
                transform: translateX(${
                  Math.floor(Math.random() * 1000) - 500
                }px);
            }
        `
    )}
    100% {
        transform: translateX(0);
    }
`;

const GlitchTextLine = styled.div.attrs(({ num }) => ({
  style: {
    // prevents many different styles from being generated
    animationDelay: `${num * -300}ms, ${Math.floor(Math.random() * -1000)}ms`,
  },
}))`
  position: absolute;
  top: 0;
  left: 0;
  padding: 0px max(3vw, 30px);

  animation: ${({ enter }) => css`
    ${GlitchClipAnimation} 3000ms linear infinite,
    ${enter
      ? GlitchChildEnterAnimation
      : GlitchChildAnimation} 500ms linear infinite;
  `};
`;

const GlitchText = ({ num, enter, children }) => (
  <div>
    {[...Array(num).keys()].map((i) => (
      <GlitchTextLine key={i} num={i} enter={enter}>
        {children}
      </GlitchTextLine>
    ))}
  </div>
);

export { GlitchText };
