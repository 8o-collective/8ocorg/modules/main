import styled, { css, keyframes } from "styled-components";

import constants from "assets/constants.js";

const JoinContainer = styled.div`
  position: absolute;

  top: 0px;
  left: 70vw;
  height: 100%;
  width: ${constants.join.width}px;
  transform: translateX(-50%);

  @media (max-width: 768px) {
    top: 58vh;
    left: 0px;
    height: ${constants.join.height}px;
    width: 100%;
    transform: translateY(-50%);
  }
`;

const JoinCanvas = styled.canvas`
  position: absolute;
  height: 100%;
  width: 100%;
  image-rendering: pixelated;
`;

const JoinButtonContainer = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  transform: translate(-50%, -50%);

  z-index: 1;
`;

const JoinOutlineFlashAnimation = keyframes`
  0% {
    border-color: red;
  }

  50% {
    border-color: white;
  }

  100% {
    border-color: red;
  }
`;

const JoinOutline = styled.div`
  position: relative;
  border: 1px solid ${({ flash }) => (flash === null ? `white` : `red`)};

  cursor: pointer;

  animation: ${({ flash }) =>
    flash &&
    css`
      ${JoinOutlineFlashAnimation} ${constants.join.button.flash /
      1000}s ease-in-out 0s 1 alternate;
    `};
`;

const JoinOutlineButton = styled.div`
  margin: auto;

  cursor: pointer;
  text-align: center;
  vertical-align: middle;
`;

export {
  JoinContainer,
  JoinCanvas,
  JoinButtonContainer,
  JoinOutline,
  JoinOutlineButton,
};
