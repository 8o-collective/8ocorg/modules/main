import styled from "styled-components";

const TearContainer = styled.div`
  position: absolute;
  top: 10vh;
  left: 73vw;
`;

const TearImage = styled.img`
  height: 20vh;
  transform: rotate(-90deg);
  filter: drop-shadow(0px 0px 1px rgba(255, 0, 0, 1));

  cursor: pointer;
`;

export { TearContainer, TearImage };
