import styled, { keyframes, css } from "styled-components";

const MissionContainer = styled.div`
  position: absolute;
  top: 25%;
  width: 75%;
  left: 0px;
  transform: translateY(-50%);

  font-size: max(3vw, 30px);
  color: white;

  display: flex;
  flex-direction: column;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

const MissionTextGlowAnimation = keyframes`
  100% {
    /* Larger blur radius */
    text-shadow:
      0 0 3px #f00,
      0 0 8px #f00,
      0 0 24px #f00,
      0 0 30px #f00,
      0 0 50px #f00;
  }
 0% {
    /* A slightly smaller blur radius */
    text-shadow:
      0 0 3px #f00,
      0 0 7px #f00,
      0 0 18px #f00,
      0 0 27px #f00,
      0 0 45px #f00;
  }
`;

const MissionSupportText = styled.div`
  color: white;
  padding: 0px max(3vw, 30px);
`;

const MissionText = styled.div`
  color: red;
  position: relative;
  padding: 0px;

  height: max(3vw, 30px);

  animation: ${({ glow }) =>
    glow &&
    css`
      ${MissionTextGlowAnimation} 0.5s ease-in-out infinite alternate
    `};
`;

export { MissionContainer, MissionSupportText, MissionText };
