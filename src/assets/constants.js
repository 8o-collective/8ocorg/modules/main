export default {
  join: {
    button: {
      outlines: 8,
      loop: 1500,
      flash: 1000,
    },
    width: 400, // px
    height: 200, // px
    unlock: 1500,
  },
  mission: {
    time: 3000,
    fadeout: 250,
    fakes: 3,
  },
};
