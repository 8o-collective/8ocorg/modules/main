import { v4 as uuidv4 } from "uuid";

const getInvite = () =>
  fetch(`${window.location.protocol}//${window.host}/api/invite`).then(
    (response) => response.json()
  );

const generateEntry = () => {
  const id = uuidv4();

  localStorage.setItem("id", id);

  getInvite().then((invite) => {
    localStorage.setItem("key", JSON.stringify(invite.key));
    localStorage.setItem("lastShardAccessed", JSON.stringify(-1));
    localStorage.setItem("encrypted", JSON.stringify(invite.encrypted));
  });
};

export default generateEntry;
