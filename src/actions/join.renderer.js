const color = `hsl(0, 100%, 50%)`; // red

function frameRenderer(join, portrait = false) {
  this.lineWidth = 2;

  if (portrait) {
    if (join.structure.draw) {
      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(
        join.structure.leftmost.last.x,
        join.structure.leftmost.last.y
      );
      this.lineTo(
        join.structure.leftmost.coords.x,
        join.structure.leftmost.coords.y
      );
      this.stroke();

      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(
        join.structure.rightmost.last.x,
        join.structure.rightmost.last.y
      );
      this.lineTo(
        join.structure.rightmost.coords.x,
        join.structure.rightmost.coords.y
      );
      this.stroke();
    } else if (join.box.draw) {
      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(
        join.box.leftmost.right.last.x,
        join.box.leftmost.right.last.y
      );
      this.lineTo(
        join.box.leftmost.right.coords.x,
        join.box.leftmost.right.coords.y
      );
      this.stroke();

      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(join.box.leftmost.down.last.x, join.box.leftmost.down.last.y);
      this.lineTo(
        join.box.leftmost.down.coords.x,
        join.box.leftmost.down.coords.y
      );
      this.stroke();

      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(
        join.box.rightmost.left.last.x,
        join.box.rightmost.left.last.y
      );
      this.lineTo(
        join.box.rightmost.left.coords.x,
        join.box.rightmost.left.coords.y
      );
      this.stroke();

      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(join.box.rightmost.up.last.x, join.box.rightmost.up.last.y);
      this.lineTo(
        join.box.rightmost.up.coords.x,
        join.box.rightmost.up.coords.y
      );
      this.stroke();
    }
  } else {
    if (join.structure.draw) {
      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(join.structure.upper.last.x, join.structure.upper.last.y);
      this.lineTo(join.structure.upper.coords.x, join.structure.upper.coords.y);
      this.stroke();

      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(join.structure.lower.last.x, join.structure.lower.last.y);
      this.lineTo(join.structure.lower.coords.x, join.structure.lower.coords.y);
      this.stroke();
    } else if (join.box.draw) {
      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(join.box.upper.down.last.x, join.box.upper.down.last.y);
      this.lineTo(join.box.upper.down.coords.x, join.box.upper.down.coords.y);
      this.stroke();

      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(join.box.upper.right.last.x, join.box.upper.right.last.y);
      this.lineTo(join.box.upper.right.coords.x, join.box.upper.right.coords.y);
      this.stroke();

      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(join.box.lower.up.last.x, join.box.lower.up.last.y);
      this.lineTo(join.box.lower.up.coords.x, join.box.lower.up.coords.y);
      this.stroke();

      this.beginPath();
      this.strokeStyle = color;
      this.moveTo(join.box.lower.left.last.x, join.box.lower.left.last.y);
      this.lineTo(join.box.lower.left.coords.x, join.box.lower.left.coords.y);
      this.stroke();
    }
  }

  // TODO: draw something when the animation is not started
  // else if (!dna.structure.draw && !dna.connector.draw) {
  // 	for (let x = 0; x < size.width; x++) {
  // 		for (let y = 0; y < size.height; y++) {
  // 			this.fillStyle = `rgba(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255}, 1.0)`;
  // 			this.fillRect( x, y, 1, 1 );
  // 		}
  // 	}
  // }
}

export default frameRenderer;
