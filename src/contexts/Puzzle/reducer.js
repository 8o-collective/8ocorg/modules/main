export const reducer = (state, action) => ({
  ...state,
  ...action,
});

export const initialState = {
  join: {
    mission: true,
  },

  stage: 0,
};
