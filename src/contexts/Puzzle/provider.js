import React, { createContext, useReducer } from "react";
import { reducer, initialState } from "./reducer.js";

export const PuzzleContext = createContext({
  state: initialState,
  dispatch: () => null,
});

export const PuzzleProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <PuzzleContext.Provider value={[state, dispatch]}>
      {children}
    </PuzzleContext.Provider>
  );
};
